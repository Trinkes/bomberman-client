#include <windows.h>
#include <tchar.h>
#include <io.h>
#include <fcntl.h>
#include <stdio.h>



HANDLE hPipeComunica;
HANDLE hPipeBroadcast;

#define PIPE_COM TEXT("\\\\192.168.1.7\\pipe\\comunica")
#define PIPE_BROAD TEXT("\\\\192.168.1.7\\pipe\\broadcast")

DWORD WINAPI lerPipe(LPVOID param);
DWORD WINAPI escreverPipe(LPVOID param);
DWORD WINAPI lerPipeDifunde(LPVOID param);

TCHAR bufa[256];
BOOL ret;

int muta = 0;

int _tmain(int argc, LPTSTR argv[]){
	TCHAR buf[256] = L"TONIIII";

	int i = 0;

	DWORD n;
	HANDLE hThreadEscreve, hThreadLer, hThreadDifunde;


#ifdef UNICODE 
	_setmode(_fileno(stdin), _O_WTEXT);
	_setmode(_fileno(stdout), _O_WTEXT);
	_setmode(_fileno(stderr), _O_WTEXT);
#endif

	//_tprintf(TEXT("[LEITOR]Conectar-me a '%s'(WaitNamedPipe)\n"), PIPE_COM);
	if (!WaitNamedPipe(PIPE_COM, NMPWAIT_WAIT_FOREVER)) {
		_tprintf(TEXT("[ERRO] Ligar ao pipe '%s'... (WaitNamedPipe)\n"), PIPE_COM);
		exit(-1);
	}

	_tprintf(TEXT("[LEITOR] Liga��o ao escritor... (CreateFile)\n"));
	hPipeComunica = CreateFile(PIPE_COM, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hPipeComunica == NULL) {
		_tprintf(TEXT("[ERRO] Ligar ao pipe '%s'... (CreateFile)\n"), PIPE_COM);
		exit(-1);
	}
	_tprintf(TEXT("[LEITOR]Liguei-me pipe Normal...\n"));

	//******************Ler difunde**********************************

	if (!WaitNamedPipe(PIPE_BROAD, NMPWAIT_WAIT_FOREVER)) {
		_tprintf(TEXT("[ERRO] Ligar ao pipe '%s'... (WaitNamedPipe)\n"), PIPE_BROAD);
		exit(-1);
	}

	_tprintf(TEXT("[LEITOR] Liga��o ao escritor... (CreateFile)\n"));
	hPipeBroadcast = CreateFile(PIPE_BROAD, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hPipeBroadcast == NULL) {
		_tprintf(TEXT("[ERRO] Ligar ao pipe '%s'... (CreateFile)\n"), PIPE_BROAD);
		exit(-1);
	}
	_tprintf(TEXT("[Cliente]Liguei-me Broadcast...\n"));
	hThreadDifunde = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)lerPipeDifunde, NULL, 0, NULL);
	
	do{
	
		TCHAR accao=NULL;
		_fgetts(bufa, sizeof(256), stdin);
	/*	switch (tolower(bufa[0]))
		{
		case 'a': accao = L"esquerda";
		break;
		case 's': accao = L"baixo";
		break;
		case 'w': accao = L"cima";
		break;
		case 'd': accao = L"direita";
		break;
		default:
		break;
		}*/
		accao = bufa;
		int tam = sizeof(accao);
		_tprintf(TEXT("[bufaaa ] : '%d'... \n"), tam);
		escreverPipe(hPipeComunica);

	} while (1);

	
	return 0;
}

/*
* Ler PIPE Broadcast
*@param Handle pipe
*/
DWORD WINAPI lerPipeDifunde(LPVOID param){
	DWORD n;
	int i;
	TCHAR buf[256];

	do {
		ret = ReadFile(hPipeBroadcast, buf, sizeof(buf), &n, NULL);
		buf[n / sizeof(TCHAR)] = '\0';
		//if (!ret || !n)
		//	break;
		_tprintf(TEXT("[Difusao] Recebi : '%s'... \n"), buf);
		
	} while (1);

}

/*
* ESCREVER PIPE NORMAL
*@param Handle pipe
*/
DWORD WINAPI escreverPipe(LPVOID param){//
	DWORD n;
	int i;
	TCHAR buf[256];

	BOOL t1 = TRUE;
	n = _tcslen(bufa)*sizeof(TCHAR);
	if (n > 2){
		WriteFile(hPipeComunica, bufa, _tcslen(bufa)*sizeof(TCHAR), &n, NULL);
		//	bufa[n / sizeof(TCHAR)] = '\0';
		_tprintf(TEXT("[CLIENTE] Enviei %s bytes ao serv... \n"), bufa);
		lerPipe(hPipeComunica);
	}
	return 0;
}
/*
* Ler PIPE NORMAL
*@param Handle pipe
*/
DWORD WINAPI lerPipe(LPVOID param){
	DWORD n;
	int i;
	TCHAR buf[256];

	ret = ReadFile(hPipeComunica, buf, sizeof(buf), &n, NULL);
	buf[n / sizeof(TCHAR)] = '\0';
	_tprintf(TEXT("\n Li : %s \n "), buf);

}
